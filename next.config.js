/** @type {import('next').NextConfig} */

module.exports = {
  reactStrictMode: true,
  experimental: {
    // Prefer loading of ES Modules over CommonJS
    esmExternals: true,
    reactRoot: true
  },
  rewrites: async () => {
    return  {
      // After checking all Next.js pages (including dynamic routes)
      // and static files, we proxy any other requests
      fallback: [
        {
          source: '/:path*',
          destination: `http://localhost:4000/:path*`,
        },
      ],
    }
  }
}