import React from 'react';
import ErrorBoundary from '../ErrorBoundary'
import App from '../App';

const Index = () => {
  return (
    <ErrorBoundary>
      <App />
    </ErrorBoundary>
  )
}

export default Index