import React, { useState, useEffect } from 'react';
import { OnePropComponentMemoized, OnePropComponent } from './Memoized';

const MemoDemo = () => {
  const [date, setDate] = useState<Date>();

  useEffect(()=>{
    const timer = setInterval(() =>
      setDate(new Date()), 1000
    )
    return clearInterval(timer);
  },[]) 
    return (
      <>
        <p>setInterval() is updating state.date once per second</p>
        state.date === {date?.toLocaleString('en-US',
          { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true })}
          
        <div>
          <OnePropComponent frequency={1} />
          <OnePropComponentMemoized frequency={1} />
        </div>

      </>
    )
}

export default MemoDemo