import React, { useState } from 'react';
import { useAuthenticateMutation } from '../generated/graphql';
import { setAccessToken } from '../accessToken';

const Login: React.FC = () => {
  const [email, setEmail] = useState('bob@bob.com');
  const [password, setPassword] = useState('bob@bob.com');
  const [login, { loading, error }] = useAuthenticateMutation({
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
    onError: (err) => {console.log(err)},
  });
  
  return (
    <div>
      Login
      <form onSubmit={async e=> {
        e.preventDefault();
        try {
          const response = await login({ variables: { email, password } });
          if (response) {
            console.log(response)
            const authenticate = response?.data?.authenticate;
            if (typeof authenticate === 'string') {
              setAccessToken(authenticate);
            } else {
              throw new Error('unable to authenticate')
            }
            setEmail('');
            setPassword('');
          }
        } catch (err) {
          console.error(err)
          throw new Error((err as Error).message)
        }
      }}>
      
      <input value={email} onChange={(e) => setEmail(e.target.value)} />
      <br />
      <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
      <br />
      <button type="submit" disabled={loading}>login</button>
      {error && <p>Error. Please try again</p>}
      </form>
    </div>
  )
}

export default Login