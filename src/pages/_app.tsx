import '../globals.css'
import type { AppProps, NextWebVitalsMetric } from 'next/app'
import { ApolloProvider } from "@apollo/client";
import client from '../Apollo';
import React from 'react';

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <ApolloProvider client={client}>
      <Component {...pageProps} />
    </ApolloProvider>
  )
}

export function reportWebVitals(metric: NextWebVitalsMetric) {
  console.log(metric)
}

export default MyApp