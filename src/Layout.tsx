import React from 'react';
import Link from 'next/link'
import { useAuth } from './useAuth';
import Spinner from './spinner';
import Hello from './Hello';
import styles from './Layout.module.css';

const Layout = () => {
  const { loading } = useAuth();

  if (loading) {
    return <Spinner />
  }

  return (
    <div className={styles.wrapper}>
      <header className={styles.header}>
        <Hello />
      </header>
      <nav className={styles.nav}>
        <ul>
          <li><Link href='/'>Home</Link></li>
          <li><Link href='/login'>Login</Link></li>
          <li><Link href='/food'>Food</Link></li>
          <li><Link href='/blahs'>Blahs</Link></li>
          <li><Link href='/memo'>React.memo</Link></li>
          <li><Link href='/hook'>Hook: useState, useEffect</Link></li>
          <li><Link href='/hook-reducer'>Hook: useReducer</Link></li>
          <li><Link href='/hook-memo'>Hook: Memo (filter)</Link></li>
          <li><Link href='/hook-deep-tree'>Hook: Passing dispatch with Context</Link></li>
          {/* <li><Link to='/lazy-suspense'>React.Suspense &amp; React.lazy</Link></li> */}
        </ul>
      </nav>
      <aside className={styles.sidebar}>
        {/* <Switch>
          <Route path='/memo' component={MemoSidebar} />
          <Route path='/hook' component={HookStateSidebar} />
          <Route path='/hook-deep-tree' component={HookDeepTreeSidebar}/>
        </Switch> */}
      </aside>
      <footer className={styles.footer}>
        &copy; 2019-2022 Patrick Skinner
    </footer>
    </div>
  )
}


export default Layout

const MemoSidebar = () =>
  <div>
    <p>The first component is a 'pure functional' component and re-renders whenever its props change, just like usual.</p>
    <p>Meanwhile we used React.memo for the second compoment. This tells React to cache based on a shallow prop comparison so React won't re-render.</p>
    <p>If a shallow comparison isn't enough, you can pass to React.memo() an equality function as an optional second param.</p>
  </div>

const HookStateSidebar = () =>
  <div>
    <p>This is the original Hook demo.</p>
    <p>useState &amp; useEffect</p>
    <p>For useEffect, to clean up a side effect e.g. setTimeout(), you can optionally return a function (e.g. clearTimeout())</p>
  </div>

const HookDeepTreeSidebar = () =>
  <div>
    <p>This is the original Hook demo.</p>
    <p>useState &amp; useEffect</p>
    <p>For useEffect, to clean up a side effect e.g. setTimeout(), you can optionally return a function (e.g. clearTimeout())</p>
  </div>
