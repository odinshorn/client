import React from 'react';
import { AuthProvider } from './useAuth';
import Layout from './Layout';

const App = () => {
  return (
    <AuthProvider>
      <Layout />
    </AuthProvider>
  )
}

export default App
